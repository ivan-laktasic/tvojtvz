CREATE SCHEMA IF NOT EXISTS tvoj_tvz;

CREATE TABLE IF NOT EXISTS tvoj_tvz.subject(
  id BIGSERIAL not NULL ,
  external_id BIGSERIAL not NULL ,
  name VARCHAR(255),
  description VARCHAR(255),
  conditions JSONB,
  literature JSONB,
  CONSTRAINT pk_subject PRIMARY KEY (id)
) ;
CREATE TABLE IF NOT EXISTS tvoj_tvz.professor (
  id BIGSERIAL not NULL ,
  external_id BIGSERIAL not NULL ,
  name VARCHAR(250),
  last_name VARCHAR(250),
  oib VARCHAR(11),
  CONSTRAINT pk_proffesor PRIMARY KEY (id)
) ;
CREATE TABLE IF NOT EXISTS tvoj_tvz.student(
  id BIGSERIAL not NULL ,
  external_id BIGSERIAL not NULL ,
  name VARCHAR(250),
  last_name VARCHAR(250),
  jmbag VARCHAR(10),
  CONSTRAINT pk_student PRIMARY KEY (id)
) ;
CREATE TABLE IF NOT EXISTS tvoj_tvz.professor_subject(
  professor_id BIGSERIAL NOT NULL ,
  subject_id BIGSERIAL NOT NULL ,
  CONSTRAINT fk_professor FOREIGN KEY (professor_id) REFERENCES  professor (id)  on delete restrict on update restrict,
  CONSTRAINT fk_subject FOREIGN KEY (subject_id) REFERENCES  subject (id)  on delete restrict on update restrict
) ;
CREATE TABLE IF NOT EXISTS tvoj_tvz.student_subject(
  student_id BIGSERIAL NOT NULL ,
  subject_id BIGSERIAL NOT NULL ,
  CONSTRAINT fk_subject FOREIGN KEY (subject_id) REFERENCES  subject (id)  on delete restrict on update restrict,
  CONSTRAINT fk_student FOREIGN KEY (student_id) REFERENCES  student (id)  on delete restrict on update restrict

);
CREATE TABLE IF NOT EXISTS tvoj_tvz.post(
  id BIGSERIAL NOT NULL ,
  subject_id BIGSERIAL NOT NULL ,
  professor_id BIGSERIAL NOT NULL,
  post_text text,
  CONSTRAINT pk_post PRIMARY KEY (id),
  CONSTRAINT fk_professor FOREIGN KEY (professor_id) REFERENCES  professor (id)  on delete restrict on update restrict,
  CONSTRAINT fk_subject FOREIGN KEY (subject_id) REFERENCES  subject (id)  on delete restrict on update restrict
);
CREATE TABLE IF NOT EXISTS tvoj_tvz.lecture_term(
  id BIGSERIAL NOT NULL,
  time_from time,
  time_to time,
  subject_id BIGSERIAL NOT NULL,
  professor_id BIGSERIAL NOT NULL,
  type INT,
  location jsonb,
  lecture_number INT,
  CONSTRAINT pk_lecture_term PRIMARY KEY (id),
  CONSTRAINT fk_professor FOREIGN KEY (professor_id) REFERENCES  professor (id)  on delete restrict on update restrict,
  CONSTRAINT fk_subject FOREIGN KEY (subject_id) REFERENCES  subject (id)  on delete restrict on update restrict
);