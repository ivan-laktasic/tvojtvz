package hr.tvz.tvojTvz.tvojTvz.repository

import hr.tvz.tvojTvz.tvojTvz.model.PostEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PostRepository: JpaRepository<PostEntity, Long> {


}