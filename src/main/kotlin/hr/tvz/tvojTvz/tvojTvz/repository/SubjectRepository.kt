package hr.tvz.tvojTvz.tvojTvz.repository

import hr.tvz.tvojTvz.tvojTvz.entity.SubjectEntity
import org.springframework.data.jpa.repository.JpaRepository

interface SubjectRepository: JpaRepository<SubjectEntity, Long> {
}