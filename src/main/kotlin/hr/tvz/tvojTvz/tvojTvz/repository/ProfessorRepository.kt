package hr.tvz.tvojTvz.tvojTvz.repository

import hr.tvz.tvojTvz.tvojTvz.entity.ProfessorEntity
import org.springframework.data.jpa.repository.JpaRepository

interface ProfessorRepository:JpaRepository<ProfessorEntity,Long> {
}