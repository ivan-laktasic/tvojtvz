package hr.tvz.tvojTvz.tvojTvz.repository

import hr.tvz.tvojTvz.tvojTvz.entity.StudentEntity
import org.springframework.data.jpa.repository.JpaRepository

interface StudentRepository: JpaRepository<StudentEntity, Long> {
}