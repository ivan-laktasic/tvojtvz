package hr.tvz.tvojTvz.tvojTvz.model

import hr.tvz.tvojTvz.tvojTvz.entity.ProfessorEntity
import hr.tvz.tvojTvz.tvojTvz.entity.SubjectEntity
import javax.persistence.*

@Entity
@Table(name = "post")
data class PostEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        var id: Long?,

        var text: String?,

        @ManyToOne
        @JoinColumn(name= "subject")
        var subject: SubjectEntity?,

        @ManyToOne
        @JoinColumn(name= "professor")
        var professor: ProfessorEntity?
) {
        fun mapToModel():Post {
                return Post(
                        id = id,
                        text = text,
                        subject = subject!!.mapToModel(),
                        professor = professor!!.mapToModel()
                )
        }


}