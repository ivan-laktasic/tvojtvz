package hr.tvz.tvojTvz.tvojTvz.entity

import hr.tvz.tvojTvz.tvojTvz.model.Professor
import hr.tvz.tvojTvz.tvojTvz.model.Student
import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
@Table(name="student")
class StudentEntity (
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        var id: Long?,

        var externalId: Long?,

        var name: String?,

        var lastName: String?,

        @Length(max = 10) var jmbag: String?,
        @ManyToMany(cascade = [(CascadeType.ALL)])
        @JoinTable(name = "student_subject",
                joinColumns = [(JoinColumn(name = "student_id", referencedColumnName = "id"))],
                inverseJoinColumns = [(JoinColumn(name = "subject_id", referencedColumnName = "id"))])
        var subjects: List<SubjectEntity>? = mutableListOf()
){
        fun mapToModel(): Student{
                return Student(
                        id=id,
                        subjects =subjects?.map { entity -> entity.mapToModel() },
                        jmbag = jmbag,
                        lastName = lastName,
                        name = name,
                        externalId = externalId
                )
        }

        fun manyToManyMap(): Student {
                return Student(
                        id=id,
                        subjects =null,
                        jmbag = jmbag,
                        lastName = lastName,
                        name = name,
                        externalId = externalId
                )
        }

}