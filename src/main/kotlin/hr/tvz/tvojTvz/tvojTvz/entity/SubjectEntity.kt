package hr.tvz.tvojTvz.tvojTvz.entity

import com.vladmihalcea.hibernate.type.array.StringArrayType
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import hr.tvz.tvojTvz.tvojTvz.model.Literature
import hr.tvz.tvojTvz.tvojTvz.model.PostEntity
import hr.tvz.tvojTvz.tvojTvz.model.Subject
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import javax.persistence.*

@TypeDefs(value = [
        TypeDef(
                name = "string-array",
                typeClass = StringArrayType::class
        ),
        TypeDef(
                name = "jsonb",
                typeClass = JsonBinaryType::class
        )
])
@Entity
@Table(name="subject")
class SubjectEntity (
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        var id: Long?,

        var externalId: Long?,

        var name: String?,

        var description: String?,

        @Type( type = "string-array" )
        @Column(columnDefinition = "text[]")
        var conditions: MutableSet<String>?,

        @Type(type = "jsonb")
        @Column(columnDefinition = "jsonb")
        var literature: Literature?,

        @ManyToMany(cascade = [(CascadeType.ALL)])
        @JoinTable(name = "student_subject",
                joinColumns = [(JoinColumn(name = "subject_id", referencedColumnName = "id"))],
                inverseJoinColumns = [(JoinColumn(name = "student_id", referencedColumnName = "id"))])
        var students: List<StudentEntity>? = mutableListOf(),

        @ManyToMany(cascade = [(CascadeType.ALL)])
        @JoinTable(name = "professor_subject",
                joinColumns = [(JoinColumn(name = "subject_id", referencedColumnName = "id"))],
                inverseJoinColumns = [(JoinColumn(name = "professor_id", referencedColumnName = "id"))])
        var professors: List<ProfessorEntity>? = mutableListOf(),

        @OneToMany(mappedBy = "subject")
        var posts: List<PostEntity>? = mutableListOf()

){
        fun mapToModel(): Subject {
                return Subject(
                        id=id,
                        externalId = externalId,
                        name = name,
                        students = students?.map { entity -> entity.manyToManyMap() },
                        professors = professors?.map { entity -> entity.manyToManyMap() },
                        description = description,
                        conditions = conditions?.toList(),
                        literature = literature
                )
        }

        fun manyToManyMap(): Subject {
                return Subject(
                        id=id,
                        externalId = externalId,
                        name = name,
                        students = students?.map { entity -> entity.mapToModel() },
                        professors = null,
                        description = description,
                        conditions = conditions?.toList(),
                        literature = literature
                )
        }
}