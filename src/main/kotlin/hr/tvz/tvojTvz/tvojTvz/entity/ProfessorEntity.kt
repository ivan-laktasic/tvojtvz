package hr.tvz.tvojTvz.tvojTvz.entity

import hr.tvz.tvojTvz.tvojTvz.model.PostEntity
import hr.tvz.tvojTvz.tvojTvz.model.Professor
import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
@Table(name = "professor")
data class ProfessorEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        var id: Long?,

        var externalId: Long?,

        var name: String?,

        var lastName: String?,

        @Length(max = 11) var oib: String?,
        @ManyToMany(cascade = [(CascadeType.ALL)])
        @JoinTable(name = "professor_subject",
                joinColumns = [(JoinColumn(name = "professor_id", referencedColumnName = "id"))],
                inverseJoinColumns = [(JoinColumn(name = "subject_id", referencedColumnName = "id"))])
        var subjects: List<SubjectEntity>? = mutableListOf(),

        @OneToMany(mappedBy = "professor", cascade = [(CascadeType.ALL)], fetch = FetchType.EAGER)
        var posts: List<PostEntity>? = mutableListOf()
){
        fun mapToModel():Professor {
                return Professor(
                        id = id,
                        subjects = subjects?.map { entity -> entity.manyToManyMap() },
                        name = name,
                        externalId = externalId,
                        lastName = lastName,
                        oib = oib,
                        posts = posts?.map { entity -> entity.mapToModel() }
                )
        }

        fun manyToManyMap():Professor {
                return Professor(
                        id = id,
                        name = name,
                        subjects = null,
                        externalId = externalId,
                        lastName = lastName,
                        oib = oib,
                        posts = posts?.map { entity -> entity.mapToModel() }
                )
        }
}