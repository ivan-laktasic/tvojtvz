package hr.tvz.tvojTvz.tvojTvz.controller

import hr.tvz.tvojTvz.tvojTvz.model.Professor
import hr.tvz.tvojTvz.tvojTvz.repository.ProfessorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/professor")
@CrossOrigin(origins = ["http://localhost:3000"])
class ProfessorController
@Autowired constructor(
        val professorRepository: ProfessorRepository
) {

    @PostMapping
    fun save(@RequestBody professor: Professor) {
        professorRepository.save(professor.mapToEntity())
    }

    @GetMapping(produces = ["application/json"])
    fun getAll():List<Professor> {
        return professorRepository.findAll().map { entity -> entity.mapToModel() }
    }

    @GetMapping("/{id}",  produces = ["application/json"])
    fun getOne(@PathVariable id:Long): Professor{
        return professorRepository.getOne(id).mapToModel()
    }

}