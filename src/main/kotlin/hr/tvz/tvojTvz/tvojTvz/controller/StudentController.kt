package hr.tvz.tvojTvz.tvojTvz.controller

import hr.tvz.tvojTvz.tvojTvz.model.Student
import hr.tvz.tvojTvz.tvojTvz.repository.StudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping( "/student")
class StudentController
@Autowired constructor(
        val studentRepository: StudentRepository
) {

    @PostMapping
    fun save(@RequestBody student: Student) {
        studentRepository.saveAndFlush(student.mapToEntity())
    }

    @GetMapping
    fun getAll():List<Student> {
        return studentRepository.findAll().map { entity -> entity.mapToModel() }
    }

}