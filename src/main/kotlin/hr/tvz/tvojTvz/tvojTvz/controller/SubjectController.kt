package hr.tvz.tvojTvz.tvojTvz.controller

import hr.tvz.tvojTvz.tvojTvz.model.Post
import hr.tvz.tvojTvz.tvojTvz.model.Subject
import hr.tvz.tvojTvz.tvojTvz.repository.ProfessorRepository
import hr.tvz.tvojTvz.tvojTvz.repository.SubjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/subject")
@CrossOrigin(origins = ["http://localhost:3000"])
class SubjectController
@Autowired constructor(
        val subjectRepository: SubjectRepository,
        val professorRepository: ProfessorRepository
) {

    @PostMapping
    fun save(@RequestBody subject: Subject) {
        val entity = subject.mapToEntity()
        entity.professors = subject.professors!!.map { professor -> professorRepository.getOne(professor.id!!)}
        subjectRepository.save(entity)
    }

    @GetMapping
    fun getAll():List<Subject> {
        return subjectRepository.findAll().map { entity -> entity.mapToModel() }
    }

    @GetMapping("/{id}")
    fun getSubject(@PathVariable id: Long):Subject {
        return subjectRepository.getOne(id).mapToModel()
    }

}