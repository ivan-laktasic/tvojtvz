package hr.tvz.tvojTvz.tvojTvz.controller

import hr.tvz.tvojTvz.tvojTvz.model.Post
import hr.tvz.tvojTvz.tvojTvz.repository.PostRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/post", produces = ["application/json"])
class PostController
@Autowired constructor(
        val postRepository: PostRepository
) {

    @PostMapping
    fun save(@RequestBody post: Post) {
        postRepository.save(post.mapToEntity())
    }

    @GetMapping
    fun getAll():List<Post> {
        return postRepository.findAll().map { entity -> entity.mapToModel() }
    }

    @GetMapping("/student/{id}")
    fun getBySubject(@PathVariable id: Long): List<Post> {
        return postRepository.findAll()
                .map { entity -> entity.mapToModel() }
                .filter { post -> post.subject!!.students!!.any { student -> student.id == id} }
    }

    @GetMapping("/professor/{id}")
    fun getByProfessor(@PathVariable id: Long): List<Post> {
        return postRepository.findAll()
                .map { entity -> entity.mapToModel() }
                .filter { post -> post.subject!!.professors!!.any { prof -> prof.id == id} }
    }

}