package hr.tvz.tvojTvz.tvojTvz

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class TvojTvzApplication

fun main(args: Array<String>) {
    runApplication<TvojTvzApplication>(*args)
}
