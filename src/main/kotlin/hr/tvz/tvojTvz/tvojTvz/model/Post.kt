package hr.tvz.tvojTvz.tvojTvz.model

data class Post(
        val id: Long?,
        val text: String?,
        val subject: Subject?,
        val professor: Professor?
    ) {

    fun mapToEntity(): PostEntity {
        return PostEntity(
                id = this.id,
                text =  this.text,
                subject =  this.subject!!.mapToEntity(),
                professor = this.professor!!.mapToEntity()
            )
    }

}