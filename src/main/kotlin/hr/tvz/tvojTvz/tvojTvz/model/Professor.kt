package hr.tvz.tvojTvz.tvojTvz.model

import hr.tvz.tvojTvz.tvojTvz.entity.ProfessorEntity

class Professor(
        val id: Long?,
        val externalId: Long?,
        val name: String?,
        val lastName: String?,
        val oib: String?,
        val subjects: List<Subject>?,
        val posts: List<Post>?
) {

    fun mapToEntity():ProfessorEntity {
        return ProfessorEntity(
                id=id,
                externalId = externalId,
                name= name,
                lastName = lastName,
                oib= oib,
                subjects = subjects?.map { subject -> subject.mapToEntity() },
                posts = posts?.map { post -> post.mapToEntity() }
        )
    }
}