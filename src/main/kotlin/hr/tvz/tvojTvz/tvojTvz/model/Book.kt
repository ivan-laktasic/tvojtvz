package hr.tvz.tvojTvz.tvojTvz.model

import java.io.Serializable

class Book(
        val writer: String,
        val title: String,
        val publisher: String,
        val year: Int
): Serializable {
}