package hr.tvz.tvojTvz.tvojTvz.model

import java.io.Serializable

class Literature(
        val literature: Set<Book>
): Serializable {
}