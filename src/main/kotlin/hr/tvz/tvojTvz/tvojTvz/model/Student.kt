package hr.tvz.tvojTvz.tvojTvz.model

import hr.tvz.tvojTvz.tvojTvz.entity.StudentEntity

class Student(
        val id: Long?,
        val externalId: Long?,
        val name: String?,
        val lastName: String?,
        val jmbag: String?,
        val subjects: List<Subject>?
) {
    fun mapToEntity(): StudentEntity {
        return StudentEntity(
                id = id,
                externalId = externalId,
                name = name,
                lastName = lastName,
                jmbag = jmbag,
                subjects = subjects?.map { subject -> subject.mapToEntity()  }
        )
    }
}