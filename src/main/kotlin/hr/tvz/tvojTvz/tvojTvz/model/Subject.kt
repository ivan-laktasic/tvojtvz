package hr.tvz.tvojTvz.tvojTvz.model

import hr.tvz.tvojTvz.tvojTvz.entity.SubjectEntity

class Subject(
        val id: Long?,
        val externalId: Long?,
        val name: String?,
        val description: String?,
        val conditions: List<String>?,
        val literature: Literature?,
        val professors: List<Professor>?,
        val students: List<Student>?
) {

    fun mapToEntity(): SubjectEntity {
        return SubjectEntity(
                id = this.id,
                externalId = this.externalId,
                name = this.name,
                description = this.description,
                literature = this.literature,
                conditions = conditions?.toMutableSet(),
                professors = professors?.map { professor ->  professor.mapToEntity()},
                students = students?.map { student -> student.mapToEntity() }
        )
    }
}